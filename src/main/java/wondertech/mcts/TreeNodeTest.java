package wondertech.mcts;

public class TreeNodeTest {
    public static void main(String[] args) {
        TreeNode tn = new TreeNode();
        int size = 10;
        for (int i=0; i<size; i++) {
            tn.selectAction();
        }
        TreeView tv = new TreeView(tn);
        tv.showTree("After " + size + " play outs");
        System.out.println("Done");
    }
}